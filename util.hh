#include "pstream.h"
#include <memory>
template <typename Self> struct counted {
  std::shared_ptr<uint64_t> s = std::make_shared<uint64_t>(1);
  counted(counted &x) : s(x.s) { (*s)++; }
  counted() {}
  ~counted() {
    (*s)--;
    Self *self = this;
    self->deconstruct();
  }
};
struct prtl_pstream : redi::pstream, counted<prtl_pstream> {
  using redi::pstream::pstream;
  inline void deconstruct() {
    auto &self = *this;
    char c;
    while (self >> c)
      ;
  }
};