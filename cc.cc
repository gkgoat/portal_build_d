#include "cc.hh"
#include "api.hh"
#include "pstream.h"
#include "util.hh"
#include <cstddef>
#include <cstdlib>
#include <exception>
#include <functional>
#include <map>
#include <memory>
#include <vector>
struct scanner : file_action {
  std::string source_file;
  std::vector<std::string> includes;
  std::string compiler;
  std::vector<std::string> flags;
  size_t hash() override {
    auto h = std::hash<std::string>{};
    size_t x = 0;
    x ^= h(source_file);
    x <<= 1;
    x ^= h(compiler);
    x <<= 1;
    for (auto i : includes) {
      x ^= h(i);
      x <<= 1;
    }
    for (auto i : flags) {
      x ^= h(i);
      x <<= 1;
    }
    return x;
  }
  void build(std::function<void(std::string)> need) override {
    need(source_file);
    std::vector<std::string> v{
        "perl", std::string(getenv("IBASE")) + "/cc_scanner.pl", source_file};
    for (auto i : includes) {
      v.push_back(i);
    }
    prtl_pstream p(v);
    std::string i;
    while (p >> i && i != "..") {
      try {
        need(i);
        p << "+\n";
      } catch (action_not_found _) {
        if (_.requested != i)
          throw _;
        p << "-\n";
      }
    }
    v = {compiler, "-E"};
    for (auto i : includes) {
      v.push_back("-I");
      v.push_back(i);
    }
    for (auto f : flags) {
      v.push_back(f);
    }
    v.push_back("-o");
    v.push_back(result);
    v.push_back(source_file);
    prtl_pstream q(v);
  }
};
struct compiler : file_action {
  std::string source_file;
  std::string compiler;
  std::vector<std::string> flags;
  std::vector<std::string> plugins;
  size_t hash() override {
    auto h = std::hash<std::string>{};
    size_t x = 0;
    x ^= h(source_file);
    x <<= 1;
    x ^= h(compiler);
    x <<= 1;
    for (auto i : plugins) {
      x ^= h(i);
      x <<= 1;
    }
    for (auto i : flags) {
      x ^= h(i);
      x <<= 1;
    }
    return x;
  }
  void build(std::function<void(std::string)> need) override {
    need(source_file);
    std::vector<std::string> v = {compiler, "-c"};
    for (auto p : plugins) {
      v.push_back(std::string("-fplugin=") + p);
    }
    for (auto f : flags) {
      v.push_back(f);
    }
    v.push_back("-o");
    v.push_back(result);
    v.push_back(source_file);
    prtl_pstream q(v);
  }
};
void cc_compile(cc_compile_args args,
                std::map<std::string, std::shared_ptr<file_action>> m,
                std::string key) {
  scanner *s = new scanner;
  s->result = args.inp + "." + key + ".i";
  s->source_file = args.inp;
  s->compiler = args.compiler;
  s->includes = args.includes;
  s->flags = args.flags;
  m[args.inp + "." + key + ".i"] =
      std::shared_ptr<file_action>((file_action *)s);
  struct compiler *c = new struct compiler;
  c->result = args.inp + "." + key + ".o";
  c->source_file = args.inp + "." + key + ".i";
  c->compiler = args.compiler;
  c->flags = args.flags;
  c->plugins = args.plugins;
  m[args.inp + "." + key + ".o"] =
      std::shared_ptr<file_action>((file_action *)c);
}