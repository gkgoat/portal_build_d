#pragma once
#include "dl.hh"
#include <exception>
#include <functional>
#include <string>
struct file_action{
    virtual void build(std::function<void(std::string)> need);
    std::string result;
    virtual ~file_action();
    virtual size_t hash();
};
struct action_not_found: std::exception{
    std::string requested;
};