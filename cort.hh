#include <memory>
namespace objc {
template <typename T> struct ObjCDestruct {
  void operator()(T *obj) { [obj release]; }
};
template <typename T> struct ObjCPtr {
  std::unique_ptr<T, ObjCDestruct<T>> dat([[T alloc] init], {});
};
} // namespace objc