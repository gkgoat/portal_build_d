#pragma once
#include <string>
#include <vector>
struct cc_compile_args {
  std::string inp;
  std::vector<std::string> includes;
  std::string compiler;
  std::vector<std::string> flags;
  std::vector<std::string> plugins;
};