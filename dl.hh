#pragma once
#include <dlfcn.h>
#include <string>
#include <memory>
struct _dl{
    void *d;
    _dl(std::string s): d(dlopen(s.c_str(),RTLD_LAZY)){}
    _dl(_dl &x) = delete;
    ~_dl(){dlclose(d);}
};
struct dl{
    std::shared_ptr<_dl> d;
    dl(std::string s): d(std::make_shared<_dl>(s)){}
    template<typename T> T &sym(std::string name){
        return dlsym(d->d, name.c_str());
    }
};