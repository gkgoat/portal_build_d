sub need{
    $n = shift;
    puts $n;
    $t = <STDIN>;
    return $t -eq "+";
}

##########################################################################
# Tidies up a path or filename, removing excess ./ and ../ parts.
# Parameters:
# 	$_ - Path or filename to tidy
sub tidypath {
	$_ = shift;
	$_ = canonpath($_);
	# Use only one type of slash
	s:\\:/:g;
	# Remove constructs like xyz/..
	while (s:[^/]+?/\.\./::) {};
	return $_;
}

##########################################################################
# Searches file include path, and returns exact filename of file
# Parameters: 
# 	$inc - text from within the #include statement
#   $file - filename the include was from
sub include_search {
	my ($inc, $file, @includepaths) = @_;
	warn "include_search scanning for $inc from $file" if $debug;

	# Try relative to the including file
	$_ = tidypath(dirname($file) . "/" . $inc);
	warn "include_search trying $_ (dirname " . dirname($file) . ")" if $debug;
	return $_ if (-e "$_");

	# Try user-specified include paths
	my $item;
	foreach $item (@includepaths)
	{
	  $_ = tidypath($item . "/" . $inc);
	  warn "include_search trying $_" if $debug;
	  return $_ if (need( "$_"));
	}

	# Try relative to current directory
	$_ = $inc;
	warn "include_search trying $_" if $debug;
	return $_ if (need (-e "$_"));

	warn "include_search failed for $inc from $file" if $debug;
	return undef;
}

@files = ();

open (INPUT, shift @ARGV);
  my $line;
  while ($line = <INPUT>)
  {
#    if ($line =~ m#^\#\s*include\s(\S+)#)
    my $regexp_include_line = '^\#\s*include\s+(\S+)';
    if ($quotetypes eq "angle") 
        { $regexp_include_line = '^\#\s*include\s+<(\S+)>'; }
    elsif ($quotetypes eq "quote") 
        { $regexp_include_line = '^\#\s*include\s+"(\S+)"'; }
    if ($line =~ m/$regexp_include_line/)
    {
      my $included = $1;

      # strip quotes & anglebrackets
	  (my $rawincluded = $included) =~ s/[\<\>"]//g;
	  my $includefile = include_search($rawincluded, $file, @ARGV);

	  if (! defined $includefile)
	  {
		  $notfound{$included . " from " . $file} = 1;
		  next;
	  }

	  if ($merge eq "directory")
	  {
		  my $from = dirname($file);
		  my $to = dirname($includefile);
		  $links{"	\"$from\" -> \"$to\"\n"} = 1 unless $from eq $to;
	  }
	  else
	  {
        push(@files, ($includefile));
       }	
    }
  }

  close INPUT;

  puts "..";

  foreach(@files){
    puts $_;
  }